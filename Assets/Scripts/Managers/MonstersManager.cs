﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonstersManager : MonoBehaviour {

    public int spawnFrequencyTurn = 4;
    public Monster prefab;
    public GameObject warningPrefab;
	public GameObject SoundManager;

    private int _turnToNextSpawn;
    private int _nextSpawnX;
    private int _nextSpawnY;
    private GameObject _lastWarning;
    private bool _pending = false;

    private List<Monster> _monsters = new List<Monster>();

    private static MonstersManager s_Instance = null;

    public static MonstersManager instance
    {
        get
        {
            return s_Instance;
        }
    }

    public bool pending
    {
        get
        {
            return _pending;
        }
    }

    public List<Monster> monsters
    {
        get
        {
            return _monsters;
        }
    }

	public void changeTurnToNextSpawn(int newTurn){
		_turnToNextSpawn = newTurn;
	}

	public void destroyEnemy(int count){
		for (int i=0; i<count; i++) {
			if (_monsters.Count>=1){
				int enemyRank = Random.Range(0,_monsters.Count);
				Monster enemyDestroyed = _monsters[enemyRank];

				_monsters.Remove(enemyDestroyed);
				SoundManager.GetComponent<SoundManager>().PlayEnemyDeath();

				BoardManager.instance.cleanMonster(enemyDestroyed.currentCellX,enemyDestroyed.currentCellY);

				GameObject.Destroy(enemyDestroyed.gameObject);
			}
		}
	}

    public void NewTurn(float speedMove)
    {
        _pending = true;

        _turnToNextSpawn--;
        if (_turnToNextSpawn == 1)
        {
            ChooseNextSpawnPoint();
            _lastWarning = GameObject.Instantiate(warningPrefab, BoardManager.instance.CellToWorld(_nextSpawnX, _nextSpawnY), Quaternion.identity) as GameObject;
        } 
        else if (_turnToNextSpawn == 0)
        {
            Monster m = GameObject.Instantiate(prefab, _lastWarning.transform.position, _lastWarning.transform.rotation) as Monster;
            m.currentCellX = _nextSpawnX;
            m.currentCellY = _nextSpawnY;
            _monsters.Add(m);
            GameObject.Destroy(_lastWarning);
            _lastWarning = null;
            _turnToNextSpawn = spawnFrequencyTurn;
			SoundManager.GetComponent<SoundManager>().PlayEnemySpawn();
        }

        _pending = false;
    }
		
	public void checkIfPlayer(){
		for (int i=0; i<_monsters.Count; i++) {
			Monster enemyChecked = _monsters[i];
				
			CellContent cellChecked = BoardManager.instance.Content(enemyChecked.currentCellX,enemyChecked.currentCellY);

			if (cellChecked.player==true){
				if(BoardManager.instance.weaponsOwned>=1){
					BoardManager.instance.weaponsOwned-=1;
					_monsters.Remove(enemyChecked);
					BoardManager.instance.cleanMonster(enemyChecked.currentCellX,enemyChecked.currentCellY);
					GameObject.Destroy(enemyChecked.gameObject);
					SoundManager.GetComponent<SoundManager>().PlayEnemyDeath();
				}else {
                    PlayerManager.instance.Death();
				}
			}
		}
	}

    void Awake()
    {
        if (s_Instance != null)
        {
            Debug.LogError("MonstersManager instance not NULL ... already initialized!!!");
            return;
        }

        s_Instance = this;

        _turnToNextSpawn = spawnFrequencyTurn;
    }

    private void ChooseNextSpawnPoint()
    {
        int side = Random.Range(0, 4);
        _nextSpawnX = 0;
        _nextSpawnY = 0;
        switch (side)
        {
            case 0:
                // left
                _nextSpawnX = 0;
                _nextSpawnY = Random.Range(0, (int)BoardManager.instance.boardSizeInCells.y);
                break;

            case 1:
                // up
                _nextSpawnY = ((int)BoardManager.instance.boardSizeInCells.y) - 1;
                _nextSpawnX = Random.Range(0, (int)BoardManager.instance.boardSizeInCells.x);
                break;

            case 2:
                // right
                _nextSpawnX = ((int)BoardManager.instance.boardSizeInCells.x) - 1;
                _nextSpawnY = Random.Range(0, (int)BoardManager.instance.boardSizeInCells.y);
                break;

            case 3:
                // down
                _nextSpawnY = 0;
                _nextSpawnX = Random.Range(0, (int)BoardManager.instance.boardSizeInCells.x);
                break;
        }
    }

}
