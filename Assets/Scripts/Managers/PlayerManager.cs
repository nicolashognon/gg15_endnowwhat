﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum InputChoice
{
    None = 0,
    Left = 1,
    Right = 2,
    Up = 3,
    Down = 4,
    EndOfChoice = 5
}

[System.Serializable]
public struct PlayerInput
{
    public KeyCode left;
    public KeyCode right;
    public KeyCode up;
    public KeyCode down;

    public bool doneThisFrame;

    public bool disabled;

    //[HideInInspector]
    public bool joystick;

    //[HideInInspector]
    public InputChoice lastChoice;

    public PlayerInput(KeyCode l, KeyCode r, KeyCode u, KeyCode d)
    {
        left = l;
        right = r;
        up = u;
        down = d;
        lastChoice = InputChoice.None;
        joystick = false;
        disabled = false;
        doneThisFrame = false;
    }
}

public class PlayerManager : MonoBehaviour
{

    [System.Serializable]
    public struct Player
    {
        public GameObject panel;
        public Image portrait;
        public Image left;
        public Image right;
        public Image up;
        public Image down;
        public Image leftArrow;
        public Image rightArrow;
        public Image upArrow;
        public Image downArrow;
        public SpriteRenderer sprite;
    }

    public float turnTime = 4f;
    public int airMax = 10;
    public int airDecRatePerTurn = 1;
    public int airIncBonus = 4;
    public Text turn;
    public Image time;
    public Image air;
    public Text weapons;

    public Player[] players;

	public float minTurnTime = 2f;
	public float turnsBetweenDrop = 10;
	public float timeDrop = 0.1f;
    public AnimationCurve arrowAnimation;

	public GameObject SoundManager;

    public PlayerInput[] inputs = {
        new PlayerInput(KeyCode.LeftArrow,KeyCode.RightArrow,KeyCode.UpArrow,KeyCode.DownArrow),
        new PlayerInput(KeyCode.J,KeyCode.L,KeyCode.I,KeyCode.K),
        new PlayerInput(KeyCode.V,KeyCode.N,KeyCode.G,KeyCode.B),
        new PlayerInput(KeyCode.Q,KeyCode.D,KeyCode.Z,KeyCode.S)
    };

    public float timeFeedbackInput = 1;
    public float speedMove = 50;

    public Color portraitDisabled = new Color(64f / 255f, 64f / 255f, 64f / 255f);
    public Color arrowBGSelected = new Color(0.5f, 0.5f, 0.5f);

    public SpriteRenderer pressanykey;

    public GameObject gameover;
    public Text gameoverText;

    private bool _started = false;
    private int _turn = 0;
    private float _timer = 0f;
    private int[] _choices = new int[5];
    private int _currentCellX = 0;
    private int _currentCellY = 0;
    private int _numberOfPlayers = 2;
    private int _majority = 2;
    private int _currentAir = 0;
    private bool _takeBonusAirThisTurn = false;
    private bool _dead = false;
    private bool _finishing = false;
    private bool _pending = false;
    private ConfigurationManager _configuration;

    private static PlayerManager s_Instance = null;

    public static PlayerManager instance
    {
        get
        {
            return s_Instance;
        }
    }

    void Awake()
    {
        if (s_Instance != null)
        {
            Debug.LogError("PlayerManager instance not NULL ... already initialized!!!");
            return;
        }

        s_Instance = this;

        if (((int)InputChoice.EndOfChoice) != _choices.Length)
        {
            Debug.LogError("Invalid size of choices array");
        }

        GameObject[] configurations = GameObject.FindGameObjectsWithTag("ConfigurationManager");
        if (configurations != null && configurations.Length > 0)
        {
            _configuration = configurations[0].GetComponent<ConfigurationManager>();
            _configuration.enabled = false;
            Debug.Log("find configuration");
            if (pressanykey != null)
            {
                pressanykey.enabled = true;
            }
        }
        else if (pressanykey != null)
        {
            pressanykey.enabled = false;
        }
    }

    // Use this for initialization
    void Start()
    {
        if (Input.GetJoystickNames().Length > 0)
        {
            for (int i = 0; i < Input.GetJoystickNames().Length; i++)
            {
                inputs[i].joystick = true;
            }

            int keyboardInput = inputs.Length - Input.GetJoystickNames().Length - 1;
            for (int i = inputs.Length - 1; i >= Input.GetJoystickNames().Length; i--)
            {
                inputs[i].left = inputs[keyboardInput].left;
                inputs[i].right = inputs[keyboardInput].right;
                inputs[i].up = inputs[keyboardInput].up;
                inputs[i].down = inputs[keyboardInput].down;
                keyboardInput--;
            }
        }

        _currentCellX = Mathf.FloorToInt(BoardManager.instance.boardSizeInCells.x / 2);
        _currentCellY = Mathf.FloorToInt(BoardManager.instance.boardSizeInCells.y / 2);
        transform.position = BoardManager.instance.CellToWorld(_currentCellX, _currentCellY);
        BoardManager.instance.PlayerMoved(_currentCellX, _currentCellY, _currentCellX, _currentCellY);

        if (_configuration != null)
        {
            if (_configuration.numberOfPlayers < 4)
            {
                if (_configuration.numberOfPlayers == 2)
                {
                    players[3].panel.SetActive(false);
                    players[3].sprite.gameObject.SetActive(false);
                }
                players[2].panel.SetActive(false);
                players[2].sprite.gameObject.SetActive(false);
            }
        }
    }

    /*void OnGUI()
    {
        if (_started)
        {
            GUI.Label(new Rect(10, 10, Screen.width - 20, 20), "Players: " + _numberOfPlayers);
            GUI.Label(new Rect(10, 30, Screen.width - 20, 20), "Round: " + _turn);
            GUI.Label(new Rect(10, 50, Screen.width - 20, 20), "Time: " + _timer);
            GUI.Label(new Rect(10, 70, Screen.width - 20, 20), "Air: " + _currentAir + " / " + airMax);
        }
        else
        {
            GUI.Label(new Rect(10, 10, Screen.width - 20, 50), "Press F2|F3|F4 to start !!!");
        }
    }*/

    // Update is called once per frame
    void Update()
    {
        if (_dead)
        {
            if (gameover.activeSelf==false)
            {
                Debug.Log("death3");
				SoundManager.GetComponent<SoundManager>().PlayDeathHero();
                gameover.SetActive(true);
                gameoverText.text = _turn.ToString();
            }

            return;
        }

        if (_finishing)
        {
            return;
        }

        // wait start is pressed
        if (!_started)
        {
            if (_configuration != null)
            {
                Restart(_configuration.numberOfPlayers);
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.F2))
                {
                    Restart(2);
                }
                else if (Input.GetKeyDown(KeyCode.F3))
                {
                    Restart(3);
                }
                else if (Input.GetKeyDown(KeyCode.F4))
                {
                    Restart(4);
                }
            }                
            return;
        }

        // started
        for (int i = 0; i < _numberOfPlayers; i++)
        {
            if (inputs[i].disabled || inputs[i].doneThisFrame)
            {
                continue;
            }

            bool inputThisFrame = false;

            if (inputs[i].joystick)
            {
                if (Input.GetButtonDown("A_" + (i + 1)) || Input.GetAxis("DPad_YAxis_" + (i + 1)) <= -0.8f)
                {
                    inputs[i].lastChoice = InputChoice.Down;
                    inputThisFrame = true;
                }
                else if (Input.GetButtonDown("B_" + (i + 1)) || Input.GetAxis("DPad_XAxis_" + (i + 1)) >= 0.8f)
                {
                    inputs[i].lastChoice = InputChoice.Right;
                    inputThisFrame = true;
                }
                else if (Input.GetButtonDown("Y_" + (i + 1)) || Input.GetAxis("DPad_YAxis_" + (i + 1)) >= 0.8f)
                {
                    inputs[i].lastChoice = InputChoice.Up;
                    inputThisFrame = true;
                }
                else if (Input.GetButtonDown("X_" + (i + 1)) || Input.GetAxis("DPad_XAxis_" + (i + 1)) <= -0.8f)
                {
                    inputs[i].lastChoice = InputChoice.Left;
                    inputThisFrame = true;
                }
            }
            else
            {
                if (Input.GetKeyDown(inputs[i].left))
                {
                    inputs[i].lastChoice = InputChoice.Left;
                    inputThisFrame = true;
                }
                else if (Input.GetKeyDown(inputs[i].right))
                {
                    inputs[i].lastChoice = InputChoice.Right;
                    inputThisFrame = true;
                }
                else if (Input.GetKeyDown(inputs[i].up))
                {
                    inputs[i].lastChoice = InputChoice.Up;
                    inputThisFrame = true;
                }
                else if (Input.GetKeyDown(inputs[i].down))
                {
                    inputs[i].lastChoice = InputChoice.Down;
                    inputThisFrame = true;
                }
            }

            if (inputThisFrame)
            {
                inputs[i].doneThisFrame = true;
				SoundManager.GetComponent<SoundManager> ().PlayVoteInput ();
                players[i].portrait.color = Color.white;
            }
        }

        _timer += Time.deltaTime;
        time.fillAmount = 1 - ((float)_timer / (float)turnTime);
        if (_timer >= turnTime)
        {
            _finishing = true;
            StartCoroutine(TurnFinished());
        }
    }

    private void Restart(int playerCount)
    {
        if (pressanykey != null)
        {
            pressanykey.enabled = false;
        }
        _started = true;

        _numberOfPlayers = playerCount;

        Debug.Log("start: " + _numberOfPlayers);

        if (_numberOfPlayers < 4)
        {
            if (_numberOfPlayers == 2)
            {
                players[2].panel.SetActive(false);
                players[2].sprite.gameObject.SetActive(false);
            }
            players[3].panel.SetActive(false);
            players[3].sprite.gameObject.SetActive(false);
        }
        if (_numberOfPlayers == 2 || _numberOfPlayers == 3)
        {
            _majority = 2;
        }
        else
        {
            _numberOfPlayers = 4;
            _majority = 3;
        }

        for (int i = 0; i < inputs.Length; i++)
        {
            inputs[i].lastChoice = InputChoice.None;
            if (i >= playerCount)
            {
                inputs[i].disabled = true;
            }
        }
        for (int i = 0; i < _choices.Length; i++)
        {
            _choices[i] = 0;
        }

        for (int i = 0; i < _numberOfPlayers; i++)
        {
            players[i].portrait.color = portraitDisabled;
        }

        _timer = 0f;
        time.fillAmount = 1;
        _turn = 1;
        _currentAir = airMax;
        air.fillAmount = 1;
        _takeBonusAirThisTurn = false;

        turn.text = _turn.ToString();
    }

    private IEnumerator TurnFinished()
    {
        yield return new WaitForSeconds(timeFeedbackInput);

        StartCoroutine(InputFeedback());

        while (_pending)
        {
            yield return null;
        }

        int maxValue = 0;
        int maxIdx = 0;
        for (int i = 0; i < _choices.Length; i++)
        {
            if (_choices[i] > maxValue)
            {
                maxValue = _choices[i];
                maxIdx = i;
            }
            _choices[i] = 0;
        }

        InputChoice choice = InputChoice.None;

        if (maxValue >= _majority)
        {
            // there is a majority
            choice = ((InputChoice)maxIdx);
        }
        // special cases
        else if (maxValue == 2 && _majority == 4)
        {
            int equalToMax = 0;
            for (int i = 0; i < _choices.Length; i++)
            {
                if (_choices[i] == maxValue)
                {
                    equalToMax++;
                }
            }
            if (equalToMax == 1)
            {
                choice = ((InputChoice)maxIdx);
            }
            else if (equalToMax == 2)
            {
                choice = inputs[Random.Range(0, _numberOfPlayers)].lastChoice;
            }
        }
        else
        {
            // need to choose a "random" input
            int idx = Random.Range(0, _numberOfPlayers);
            choice = inputs[idx].lastChoice;
        }

        StartCoroutine(Move(choice));

        while (_pending)
        {
            yield return null;
        }

        yield return new WaitForSeconds(timeFeedbackInput);

        StartCoroutine(MoveMonsters());

        while (_pending)
        {
            yield return null;
        }

        MonstersManager.instance.NewTurn(speedMove);
        BoardManager.instance.NewTurn();

        _timer = 0;
        time.fillAmount = 1;

        if (!_takeBonusAirThisTurn)
        {
            _currentAir -= airDecRatePerTurn;
            if (_currentAir <= 0)
            {
                air.fillAmount = 0;
                Death();
            }
            else
            {
                air.fillAmount = (float)_currentAir / (float)airMax;
            }
        }
        else
        {
            _currentAir += airIncBonus;
            if (_currentAir > airMax)
            {
                _currentAir = airMax;
                air.fillAmount = 1;
            }
            else
            {
                air.fillAmount = (float)_currentAir / (float)airMax;
            }
            _takeBonusAirThisTurn = false;
        }

        if (!_dead)
        {
            _turn++;

            // IF TURN IS A 5 MULTIPLE, PLAY REWARD SOUND
            if (_turn % 5 == 0)
            {
                SoundManager.GetComponent<SoundManager>().PlayCelebrateTurn();
            }

            // CHANGE TIME BETWEEN TURNS
            if (_turn % turnsBetweenDrop == 0)
            {
                if (turnTime - timeDrop >= minTurnTime)
                {
                    turnTime -= timeDrop;
                    //Debug.Log ("DROP: "+turnTime);
                }
            }
            // CHANGE TIME BETWEEN TURNS
            if (_turn == 3)
            {
                MonstersManager.instance.changeTurnToNextSpawn(4);
            }
            if (_turn == 13)
            {
                MonstersManager.instance.changeTurnToNextSpawn(3);
            }

            turn.text = _turn.ToString();
            StartCoroutine(AnimateRound());
            while (_pending)
            {
                yield return null;
            }
        }

        for (int i = 0; i < _numberOfPlayers; i++)
        {
            if (inputs[i].disabled)
            {
                continue;
            }

            inputs[i].lastChoice = InputChoice.None;
            inputs[i].doneThisFrame = false;
        }

        weapons.text = BoardManager.instance.weaponsOwned.ToString();
        for (int i = 0; i < _numberOfPlayers; i++)
        {
            players[i].portrait.color = portraitDisabled;
            players[i].left.color = Color.white;
            players[i].right.color = Color.white;
            players[i].up.color = Color.white;
            players[i].down.color = Color.white;
        }

        _finishing = false;
    }

    private IEnumerator MoveMonsters()
    {
        _pending = true;
        bool loop = true;
        int count = 0;

        count = 0;
        foreach (var m in MonstersManager.instance.monsters)
        {
            if (m.InitMove())
            {
                count++;
            }
        }

        if (count != 0)
        {
            while (loop)
            {
                count = 0;
                foreach (var m in MonstersManager.instance.monsters)
                {
                    if (m.Move(speedMove))
                    {
                        count++;
                    }
                }

                if (count == 0)
                {
                    break;
                }

                yield return null;
            }
        }

        _pending = false;
    }

    private IEnumerator InputFeedback()
    {
        for (int i = 0; i < _numberOfPlayers; i++)
        {
            if (inputs[i].disabled)
            {
                continue;
            }

            _choices[((int)inputs[i].lastChoice)]++;

            Image arrow = null;

            switch (inputs[i].lastChoice)
            {
                case InputChoice.Left:
                    players[i].left.color = arrowBGSelected;
                    arrow = players[i].leftArrow;
                    break;
                case InputChoice.Right:
                    players[i].right.color = arrowBGSelected;
                    arrow = players[i].rightArrow;
                    break;
                case InputChoice.Up:
                    players[i].up.color = arrowBGSelected;
                    arrow = players[i].upArrow;
                    break;
                case InputChoice.Down:
                    players[i].down.color = arrowBGSelected;
                    arrow = players[i].downArrow;
                    break;
            }

            if (arrow != null)
            {
                _pending = true;
                StartCoroutine(AnimateArrow(arrow));
                while (_pending)
                {
                    yield return null;
                }
            }
        }
    }

    private IEnumerator AnimateArrow(Image arrow)
    {
        float timeCounter = 0;
        Keyframe[] k = arrowAnimation.keys;
        float endTime = k[k.Length - 1].time;
        while (timeCounter < endTime)
        {
            float val = arrowAnimation.Evaluate(timeCounter);
            arrow.transform.localScale = new Vector2(val,val);
            timeCounter += Time.deltaTime;
            yield return null;
        }

        _pending = false;
    }

    private IEnumerator AnimateRound()
    {
        float timeCounter = 0;
        Keyframe[] k = arrowAnimation.keys;
        float endTime = k[k.Length - 1].time;
        while (timeCounter < endTime)
        {
            float val = arrowAnimation.Evaluate(timeCounter);
            turn.transform.localScale = new Vector2(val, val);
            timeCounter += Time.deltaTime;
            yield return null;
        }

        _pending = false;
    }

    private IEnumerator Move(InputChoice iChoice)
    {
        _pending = true;

		SoundManager.GetComponent<SoundManager> ().PlayHeroWalk ();

        int oldX = _currentCellX;
        int oldY = _currentCellY;

        Vector2 position = Vector2.zero;

        switch (iChoice)
        {
            case InputChoice.None:
                _pending = false;
                yield break;

            case InputChoice.Left:
                if (_currentCellX > 0)
                {
                    _currentCellX--;
                    position = BoardManager.instance.CellToWorld(_currentCellX, _currentCellY);
                }
                break;

            case InputChoice.Right:
                if (_currentCellX < (BoardManager.instance.boardSizeInCells.x-1))
                {
                    _currentCellX++;
                    position = BoardManager.instance.CellToWorld(_currentCellX, _currentCellY);
                }
                break;

            case InputChoice.Up:
                if (_currentCellY < (BoardManager.instance.boardSizeInCells.y-1))
                {
                    _currentCellY++;
                    position = BoardManager.instance.CellToWorld(_currentCellX, _currentCellY);
                }
                break;

            case InputChoice.Down:
                if (_currentCellY > 0)
                {
                    _currentCellY--;
                    position = BoardManager.instance.CellToWorld(_currentCellX, _currentCellY);
                }
                break;
        }

        if (oldX == _currentCellX && oldY == _currentCellY)
        {

        }
        else
        {
            float startTime = Time.time;
            Vector2 startPosition = transform.position;
            float journeyLength = Vector2.Distance(startPosition, position);

            while (true)
            {
                var distCovered = (Time.time - startTime) * speedMove;
                var fracJourney = distCovered / journeyLength;
                transform.position = Vector3.Lerp(startPosition, position, fracJourney);

                if (Vector2.Distance(transform.position, position) < 0.1f)
                {
                    break;
                }

                yield return null;
            }

        }

        _takeBonusAirThisTurn = (BoardManager.instance.PlayerMoved(_currentCellX, _currentCellY, oldX, oldY).collectible == Collectible.Bonus_Air) ? true : false;

        _pending = false;
    }

    public void Death()
    {
        /*Debug.Log("death");
        gameover.gameObject.SetActive(true);
        gameoverText.text = _turn.ToString();*/
        _dead = true;
    }

    public void Relaunch()
    {
        Application.LoadLevel(0);
    }
}
