﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeTime : MonoBehaviour {

	public int maxLife;
	public int currentLife;
	public int currentX;
	public int currentY;

    private Text _text;

	// Use this for initialization
	void Awake () {
		currentLife = maxLife;
        _text = gameObject.GetComponentInChildren<Text>();
        if (_text != null)
        {
            _text.text = currentLife.ToString();
        }
	}
	
	// Update is called once per frame
	public void decreaseLife(){
		currentLife -= 1;
        if (_text != null)
        {
            _text.text = currentLife.ToString();
        }
    }
}
