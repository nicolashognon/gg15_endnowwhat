﻿using UnityEngine;
using System.Collections;

public class ConfigurationManager : MonoBehaviour {

    public float loadLevelTimer = 0.5f;
    public GameObject screen1;
    public GameObject screen2;

    public int numberOfPlayers
    {
        get
        {
            return _numberOfPlayers;
        }
    }

    private int _numberOfPlayers = 4;
    private bool _loaded = false;
    private bool _controls = false;

	// Use this for initialization
	void Awake () {
        DontDestroyOnLoad(gameObject);
	}

    void Start()
    {
        screen1.SetActive(true);
        screen2.SetActive(false);
    }

    // Update is called once per frame
	void Update () {
        if (_loaded)
        {
            return;
        }

        if (_controls)
        {
            if (Input.anyKeyDown)
            {
                _loaded = true;
                Invoke("LoadGame", loadLevelTimer);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.F2))
            {
                _numberOfPlayers = 2;
                screen1.SetActive(false);
                screen2.SetActive(true);
                _controls = true;
            }
            else if (Input.GetKeyDown(KeyCode.F3))
            {
                _numberOfPlayers = 3;
                screen1.SetActive(false);
                screen2.SetActive(true);
                _controls = true;
            }
            else if (Input.GetKeyDown(KeyCode.F4))
            {
                _numberOfPlayers = 4;
                screen1.SetActive(false);
                screen2.SetActive(true);
                _controls = true;
            }
        }	
	}

    private void LoadGame()
    {
        Application.LoadLevel(1);
    }

}
