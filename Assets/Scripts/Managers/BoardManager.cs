﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Collectible
{
    None,
    Bonus_Air,
    Bonus_Weapon,
    Bonus_Kill2
}

public struct CellContent
{
    public bool player;
    public bool monster;
    public bool city;
    public Collectible collectible;
    public GameObject collectibleGO;
}

public class BoardManager : MonoBehaviour {

    public SpriteRenderer boardSprite;
    public Vector2 boardSizeInCells;
    public Vector2 cellSizeInPixels = new Vector2(60, 60);
    public GameObject cityPrefab;
	public GameObject airPrefab;
	public GameObject weaponPrefab;
	public GameObject weapon2Prefab;


    private CellContent[,] _content;
	private List<Vector2> _cities=new List<Vector2>();
	private List<GameObject> _collectibles = new List<GameObject> ();
    private int miscAmount = 0;
    private int airAmount = 0;
	public int weaponsOwned=0;

	public GameObject SoundManager;

    private static BoardManager s_Instance = null;

    public static BoardManager instance
    {
        get
        {
            return s_Instance;
        }
    }

    public CellContent Content(int x, int y)
    {
        return _content[x, y];
    }

    public Vector2 CellToWorld(int x, int y)
    {
        if (x < 0 || x >= boardSizeInCells.x || y < 0 || y >= boardSizeInCells.y)
        {
            return Vector2.zero;
        }

        return new Vector2(
            cellSizeInPixels.x * (x - Mathf.FloorToInt(boardSizeInCells.x / 2f)),
            cellSizeInPixels.y * (y - Mathf.FloorToInt(boardSizeInCells.y / 2f))
        );
    }

	public void monsterMoved(int currentX, int currentY, int oldX, int oldY){
		_content[oldX, oldY].monster = false;
		_content[currentX, currentY].monster = true;
	}

    public CellContent PlayerMoved(int currentX, int currentY, int oldX, int oldY)
    {
        _content[oldX, oldY].player = false;
        CellContent currentContent = _content[currentX, currentY];
        _content[currentX, currentY].player = true;

		// CHECK IF THERE IS A COLLECTIBLE

		// AIR
		if (_content [currentX, currentY].collectible == Collectible.Bonus_Air) {
			_collectibles.Remove(_content[currentX, currentY].collectibleGO);
			GameObject.Destroy(_content [currentX, currentY].collectibleGO);
			_content [currentX, currentY].collectible=Collectible.None;
			_content [currentX, currentY].collectibleGO=null;
			airAmount-=1;
			SoundManager.GetComponent<SoundManager>().PlayGetAir();
		}
		// WEAPON
		if (_content [currentX, currentY].collectible == Collectible.Bonus_Weapon) {
			weaponsOwned+=1;
			_collectibles.Remove(_content[currentX, currentY].collectibleGO);
			GameObject.Destroy(_content [currentX, currentY].collectibleGO);
			_content [currentX, currentY].collectible=Collectible.None;
			_content [currentX, currentY].collectibleGO=null;
			miscAmount-=1;
			SoundManager.GetComponent<SoundManager>().PlayGetWeapon();
		}
		// WEAPON2
		if (_content [currentX, currentY].collectible == Collectible.Bonus_Kill2) {

			MonstersManager.instance.destroyEnemy(2);

			_collectibles.Remove(_content[currentX, currentY].collectibleGO);
			GameObject.Destroy(_content [currentX, currentY].collectibleGO);
			_content [currentX, currentY].collectible=Collectible.None;
			_content [currentX, currentY].collectibleGO=null;
			miscAmount-=1;
			SoundManager.GetComponent<SoundManager>().PlayGetWeapon();
		}

		// RESPAWN COLLECTIBLES SI NECESSAIRES
		respawnCollectibles ();

		// CHECK IF ZOMBIES ARE ON THE SAME SQUARE
		MonstersManager.instance.checkIfPlayer();

        return currentContent;
    }

	public void cleanMonster(int posX,int posY){
		// POUR CLEANER UNE CASE D'UN MONSTRE APRES SA DESTRUCTION
		_content[posX, posY].monster = false;
	}

	public void respawnCollectibles(){
		// SI IL Y A MOINS DE 3 COLLECTIBLES SUR LE TERRAIN
		while (miscAmount + airAmount < 3) {
			if(airAmount==0){
				// INITIALIZE A AIR COLLECTIBLE
				SpawnAir ();
			}
			else if(airAmount==1){
				// INITIALIZE A RANDOM COLLECTIBLE
				int random = Random.Range (0, 2);
				if (random == 0) {
					SpawnAir ();
				} else {
					SpawnMisc ();
				}
			}
			else if(airAmount==2){
				// INITIALIZE A MISC COLLECTIBLE
				SpawnMisc ();
			}
		}
	}

    public void NewTurn()
    {
		// CHECK IF ZOMBIES ARE ON THE SAME SQUARE
		MonstersManager.instance.checkIfPlayer();

		List <GameObject> toDestroy = new List<GameObject> ();
		foreach(GameObject collectible in _collectibles){
			collectible.GetComponent<LifeTime>().decreaseLife();
			if (collectible.GetComponent<LifeTime>().currentLife<=0){
				toDestroy.Add(collectible);
			}
		}
		foreach(GameObject collectible in toDestroy){
			_collectibles.Remove(collectible);
			GameObject.Destroy(_content [collectible.GetComponent<LifeTime>().currentX, collectible.GetComponent<LifeTime>().currentY].collectibleGO);

			if(_content [collectible.GetComponent<LifeTime>().currentX, collectible.GetComponent<LifeTime>().currentY].collectible==Collectible.Bonus_Air){
				airAmount-=1;
			}else{
				miscAmount-=1;
			}
			_content [collectible.GetComponent<LifeTime>().currentX, collectible.GetComponent<LifeTime>().currentY].collectible=Collectible.None;
			_content [collectible.GetComponent<LifeTime>().currentX, collectible.GetComponent<LifeTime>().currentY].collectibleGO=null;
		}
		toDestroy.Clear ();

		// TO DO: ADD TEMPO BEFORE REPSAWN

		respawnCollectibles ();
    }

    // Use this for initialization
	void Awake () {
        if (s_Instance != null)
        {
            Debug.LogError("BoardManager instance not NULL ... already initialized!!!");
            return;
        }

        s_Instance = this;

        _content = new CellContent[(int)boardSizeInCells.x, (int)boardSizeInCells.y];
        for (int i = 0; i < (int)boardSizeInCells.x; i++)
        {
            for (int j = 0; j < (int)boardSizeInCells.y; j++)
            {
                _content[i, j].city = false;
                _content[i, j].player = false;
                _content[i, j].monster = false;
                _content[i, j].collectible = Collectible.None;
            }
        }

            InitializeCities();
			InitializeCollectibles ();
	}

    private void InitializeCities()
    {
        int centerCellX = Mathf.FloorToInt(BoardManager.instance.boardSizeInCells.x / 2);
        int centerCellY = Mathf.FloorToInt(BoardManager.instance.boardSizeInCells.y / 2);

        // lower left
        int x = Random.Range(1, centerCellX-1);
        int y = Random.Range(1, centerCellY-1);
		_content[x, y].city = true;
        GameObject.Instantiate(cityPrefab, CellToWorld(x, y), Quaternion.identity);
		_cities.Add(new Vector2(x,y));

        // up left
        x = Random.Range(1, centerCellX - 1);
        y = Random.Range(centerCellY + 2, (int)boardSizeInCells.y-1);
        _content[x, y].city = true;
        GameObject.Instantiate(cityPrefab, CellToWorld(x, y), Quaternion.identity);
		_cities.Add(new Vector2(x,y));

        // up right
        x = Random.Range(centerCellX + 2, (int)boardSizeInCells.x-1);
        y = Random.Range(centerCellY + 2, (int)boardSizeInCells.y-1);
        _content[x, y].city = true;
        GameObject.Instantiate(cityPrefab, CellToWorld(x, y), Quaternion.identity);
		_cities.Add(new Vector2(x,y));

        // down right
        x = Random.Range(centerCellX + 2, (int)boardSizeInCells.x-1);
        y = Random.Range(1, centerCellY - 1);
        _content[x, y].city = true;
        GameObject.Instantiate(cityPrefab, CellToWorld(x, y), Quaternion.identity);
		_cities.Add(new Vector2(x,y));
    }

	// to spawn colllectibles at the start of the game
	private void InitializeCollectibles()
	{
		// INITIALIZE AIR
		SpawnAir ();

		// INITIALIZE MISC
		SpawnMisc ();

		// INITIALIZE A RANDOM COLLECTIBLE
		int random = Random.Range (0, 2);
		if (random == 0) {
			SpawnAir ();
				} else {
			SpawnMisc ();
		}

	}

	// TO SPAWN A AIR COLLECTIBLE
	private void SpawnAir(){
		while (true) {
			int index = Random.Range (0, _cities.Count);
			Vector2 cell = _cities [index];
			
			if (_content [(int)cell.x, (int)cell.y].collectible == Collectible.None
			    && _content [(int)cell.x, (int)cell.y].player==false){
				_content [(int)cell.x, (int)cell.y].collectible = Collectible.Bonus_Air;
                _content[(int)cell.x, (int)cell.y].collectibleGO = GameObject.Instantiate(airPrefab, CellToWorld((int)cell.x, (int)cell.y), Quaternion.identity) as GameObject;
				_collectibles.Add(_content[(int)cell.x, (int)cell.y].collectibleGO);
				_content [(int)cell.x, (int)cell.y].collectibleGO.GetComponent<LifeTime>().currentX=(int)cell.x;
				_content [(int)cell.x, (int)cell.y].collectibleGO.GetComponent<LifeTime>().currentY=(int)cell.y;
				SoundManager.GetComponent<SoundManager>().PlayItemSpawn();
				airAmount+=1;
				break;
			}
		}
	}

	// TO SPAWN A MISC COLLECTIBLE
	private void SpawnMisc(){
		int misc = Random.Range (0, 2);
		
		while (true) {
			int x = Random.Range(1, Mathf.FloorToInt(BoardManager.instance.boardSizeInCells.x)-1);
			int y = Random.Range(1, Mathf.FloorToInt(BoardManager.instance.boardSizeInCells.x)-1);
			
			if (_content [x, y].collectible == Collectible.None
			    && _content [x, y].city == false
			    && _content [x, y].player==false) {
				if (misc==0){
					_content [x, y].collectible = Collectible.Bonus_Weapon;
                    _content [x, y].collectibleGO = GameObject.Instantiate (weaponPrefab, CellToWorld (x, y), Quaternion.identity) as GameObject;
					_collectibles.Add(_content[x,y].collectibleGO);
					_content [x, y].collectibleGO.GetComponent<LifeTime>().currentX=x;
					_content [x, y].collectibleGO.GetComponent<LifeTime>().currentY=y;
					SoundManager.GetComponent<SoundManager>().PlayItemSpawn();
					miscAmount+=1;
					break;
				}
				if (misc==1){
					_content [x, y].collectible = Collectible.Bonus_Kill2;
                    _content[x, y].collectibleGO = GameObject.Instantiate(weapon2Prefab, CellToWorld(x, y), Quaternion.identity) as GameObject;
					_collectibles.Add(_content[x,y].collectibleGO);
					_content [x, y].collectibleGO.GetComponent<LifeTime>().currentX=x;
					_content [x, y].collectibleGO.GetComponent<LifeTime>().currentY=y;
					SoundManager.GetComponent<SoundManager>().PlayItemSpawn();
					miscAmount+=1;
					break;
				}
			}
		}
	}
}
