﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioClip HeroWalk;
	public AudioClip EnemyWalk;
	public AudioClip GetAir;
	public AudioClip ItemSpawn;
	public AudioClip VoteInput;
	public AudioClip GetWeapon;
	public AudioClip EnemySpawn;
	public AudioClip EnemyDeath;
	public AudioClip CelebrateTurn;
	public AudioClip DeathHero;
	
	// QUAND LE PERSONNAGE SE DEPLACE => DONE
	public void PlayHeroWalk(){
		audio.clip = HeroWalk;
		audio.Play();
	}

	// QUAND UN ENNEMI SE DEPLACE => DONE
	public void PlayEnemyWalk(){
		audio.clip = EnemyWalk;
		audio.Play();
	}

	// QUAND UN OBJET SPAWNE => DONE
	public void PlayItemSpawn(){
		audio.clip = ItemSpawn;
		audio.Play();
	}

	// QUAND UN JOUEUR RECUPERE UNE ARME => DONE
	public void PlayGetWeapon(){
		audio.clip = GetWeapon;
		audio.Play();
	}

	// QUAND UN JOUEUR RECUPERE DE l'AIR => DONE
	public void PlayGetAir(){
		audio.clip = GetAir;
		audio.Play();
	}

	// QUAND UN JOUEUR VOTE => DONE
	public void PlayVoteInput(){
		audio.clip = VoteInput;
		audio.Play();
	}

	// QUAND UN ENNEMI SPAWNE => DONE
	public void PlayEnemySpawn(){
		audio.clip = EnemySpawn;
		audio.Play();
	}

	// QUAND UN ENNEMI MEURT => DONE
	public void PlayEnemyDeath(){
		audio.clip = EnemyDeath;
		audio.Play();
	}

	// TOUR MULTIPLE DE 5
	public void PlayCelebrateTurn(){
		audio.clip = CelebrateTurn;
		audio.Play();
	}

	// MORT DU HEROS
	public void PlayDeathHero(){
		audio.clip = DeathHero;
		audio.Play();
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
