﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour
{

    public int currentCellX;
    public int currentCellY;

    float startTime;
    Vector2 startPosition;
    float journeyLength;
    Vector2 target = Vector2.zero;

    public bool InitMove()
    {
        // JOUER SON ENNEMI QUI MARCHE
        GameObject SoundManager = GameObject.Find("SoundManager");
        SoundManager.GetComponent<SoundManager>().PlayEnemyWalk();

        int oldX = currentCellX;
        int oldY = currentCellY;

        target = Vector2.zero;

        if (CheckOneCell())
        {
            target = BoardManager.instance.CellToWorld(currentCellX, currentCellY);
        }
        else if (CheckTwoCells())
        {
            target = BoardManager.instance.CellToWorld(currentCellX, currentCellY);
        }
        else
        {
            // random move
            // choose x or y
            if (System.DateTime.Now.Second % 2 == 0)
            {
                // move x
                if (System.DateTime.Now.Millisecond % 2 == 0 && currentCellX > 0)
                {
                    currentCellX--;
                }
                else if (currentCellX < ((int)BoardManager.instance.boardSizeInCells.x - 1))
                {
                    currentCellX++;
                }
                else
                {
                    currentCellX--;
                }
            }
            else
            {
                // move y
                if (System.DateTime.Now.Millisecond % 2 == 0 && currentCellY > 0)
                {
                    currentCellY--;
                }
                else if (currentCellY < ((int)BoardManager.instance.boardSizeInCells.y - 1))
                {
                    currentCellY++;
                }
                else
                {
                    currentCellY--;
                }
            }

            target = BoardManager.instance.CellToWorld(currentCellX, currentCellY);
        }

        startTime = Time.time;

        startPosition = transform.position;
        journeyLength = Vector2.Distance(startPosition, target);

        if ((currentCellX - oldX) < 0)
        {
            transform.localScale = new Vector2(-1, 1);
        }
        else if ((currentCellX - oldX) > 0)
        {
            transform.localScale = new Vector2(1, 1);
        }

        return Mathf.Approximately(journeyLength, 0) == false ? true : false;
    }

    public bool Move(float speedMove)
    {
        // Distance moved = time * speed.
        var distCovered = (Time.time - startTime) * speedMove;

        // Fraction of journey completed = current distance divided by total distance.
        var fracJourney = distCovered / journeyLength;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(startPosition, target, fracJourney);

        if (Vector2.Distance(transform.position, target) < 0.1f)
        {
            return false;
        }

        return true;
    }

    private bool CheckOneCell()
    {
        if (currentCellX > 0)
        {
            if (BoardManager.instance.Content(currentCellX - 1, currentCellY).player)
            {
                // player is one cell left
                currentCellX--;
                return true;
            }
        }
        if (currentCellX < ((int)BoardManager.instance.boardSizeInCells.x - 1))
        {
            if (BoardManager.instance.Content(currentCellX + 1, currentCellY).player)
            {
                // player is one cell right
                currentCellX++;
                return true;
            }
        }
        if (currentCellY > 0)
        {
            if (BoardManager.instance.Content(currentCellX, currentCellY - 1).player)
            {
                // player is one cell down
                currentCellY--;
                return true;
            }
        }
        if (currentCellY < ((int)BoardManager.instance.boardSizeInCells.y - 1))
        {
            if (BoardManager.instance.Content(currentCellX, currentCellY + 1).player)
            {
                // player is one cell up
                currentCellY++;
                return true;
            }
        }

        return false;
    }

    private bool CheckTwoCells()
    {
		// 2 CASES HAUT
		if (currentCellY < ((int)BoardManager.instance.boardSizeInCells.y - 2)) {
			if (BoardManager.instance.Content (currentCellX, currentCellY+2).player) {
				currentCellY++;
				return true;
			}
		}
		
		// 2 CASES BAS
		if (currentCellY > 1) {
			if (BoardManager.instance.Content (currentCellX, currentCellY-2).player) {
				currentCellY--;
				return true;
			}
		}
		
		// 2 CASES DROITE
		if (currentCellX < ((int)BoardManager.instance.boardSizeInCells.x - 2)) {
			if (BoardManager.instance.Content (currentCellX+2, currentCellY).player) {
				currentCellX++;
				return true;
			}
		}
		
		// 2 CASES GAUCHE
		if (currentCellX > 1) {
			if (BoardManager.instance.Content (currentCellX-2, currentCellY).player) {
				currentCellX--;
				return true;
			}
		}

        if (currentCellX > 0)
        {
            if (currentCellY > 0)
            {
                if (BoardManager.instance.Content(currentCellX - 1, currentCellY - 1).player)
                {
                    // player is two cell from the player left/down
                    // TODO choose if we move X or Y depending on obstacles
					int choseDirection = Random.Range(0,2);
					if(choseDirection==1){
                    	currentCellY--;
					}else{
						currentCellX--;
					}
                    return true;
                }
            }
            if (currentCellY < ((int)BoardManager.instance.boardSizeInCells.y - 1))
            {
                if (BoardManager.instance.Content(currentCellX - 1, currentCellY + 1).player)
                {
                    // player is two cell from the player left/up
                    // TODO choose if we move X or Y depending on obstacles
					int choseDirection = Random.Range(0,2);
					if(choseDirection==1){
						currentCellY++;
					}else{
						currentCellX--;
					}
                    return true;
                }
            }
        }

        if (currentCellX < ((int)BoardManager.instance.boardSizeInCells.x - 1))
        {
            if (currentCellY > 0)
            {
                if (BoardManager.instance.Content(currentCellX + 1, currentCellY - 1).player)
                {
                    // player is two cell from the player left/down
                    // TODO choose if we move X or Y depending on obstacles
					int choseDirection = Random.Range(0,2);
					if(choseDirection==1){
						currentCellY--;
					}else{
						currentCellX++;
					}
                    return true;
                }
            }
            if (currentCellY < ((int)BoardManager.instance.boardSizeInCells.y - 1))
            {
                if (BoardManager.instance.Content(currentCellX + 1, currentCellY + 1).player)
                {
                    // player is two cell from the player left/up
                    // TODO choose if we move X or Y depending on obstacles
					int choseDirection = Random.Range(0,2);
					if(choseDirection==1){
						currentCellY++;
					}else{
						currentCellX++;
					}
                    return true;
                }
            }
        }

        return false;
    }
}
