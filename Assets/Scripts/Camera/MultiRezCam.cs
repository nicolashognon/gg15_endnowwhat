﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
[AddComponentMenu("Chapo/MultiRezCam")]
public class MultiRezCam : MonoBehaviour
{

	[SerializeField]
	protected MultiRezCam _inheritSettings;

	public MultiRezCam inheritSettings
	{
		get { return _inheritSettings; }
		set { _inheritSettings = value; CheckInheritLoop(); _UpdateCamera();} 
	}

	public enum FitMode
	{
		none,
		fitWidth,
		fitHeight,
		fitEntire,
		fitToFill
	}
		
	protected Camera thisCamera;

	void CheckInheritLoop()
	{
		if( _inheritSettings == null )
			return;
			
		List<MultiRezCam> mrcs = new List<MultiRezCam>(1);
		MultiRezCam cam = this;
		while( cam.inheritSettings != null)
		{
			mrcs.Add (cam);
			if( mrcs.Contains(cam.inheritSettings) )
			{
				Debug.Log("inherit loop detected : fixing it by setting '" + gameObject.name+"' inheritSetting to NULL");
				cam.inheritSettings = null;
			}
			else
			{
				cam = cam.inheritSettings;
			}
		}
	}

	void Awake()
	{
		CheckInheritLoop();

		UpdateViewPortRez();
		if( ! thisCamera.isOrthoGraphic )
		{
			Debug.Log ("Camera"+thisCamera.gameObject.name+" should be orthographic");
		}
	}

	void OnEnable()
	{
		_UpdateCamera();
	}

	protected virtual void UpdateViewPortRez()
	{
		thisCamera = camera;
		if( thisCamera.targetTexture == null )
			_viewportResolution = new Vector2(Screen.width,Screen.height);
		else
			_viewportResolution = new Vector2(thisCamera.targetTexture.width,thisCamera.targetTexture.height);
	}


	[SerializeField]
	public int _pixelsToUnits = 100;

	public int pixelsToUnits
	{
		get { return settingsCam._pixelsToUnits; }
	}

	protected Vector2 _viewportResolution = Vector2.zero;
	public Vector2 viewportResolution
	{
		get {
			if( _viewportResolution == Vector2.zero )
			{
				UpdateViewPortRez();
			}
			return _viewportResolution; 
		}

	}

	[SerializeField]
	FitMode _fitMode;

	public FitMode fitMode
	{
		get { return settingsCam._fitMode; }
		set { _fitMode = value; _cameraDirty = true; }
	}

	public bool useReferenceResolution
	{
		get { return _fitMode != FitMode.none; } 
	}

	[SerializeField]
	protected Vector2 _referenceResolution;

	MultiRezCam settingsCam
	{
		get { return inheritSettings != null ? inheritSettings.settingsCam : this; }
	}
		
	public Vector2 referenceResolution
	{
		get {
			return settingsCam._referenceResolution; 
		}
		set { _referenceResolution = value;_cameraDirty = true;}
	}


	float _currentScale = 1;

	public float currentScale
	{
		get {return _currentScale;}
	}
		
	public Vector2 worldResolution
	{
		get { 
			if(crop)
				return referenceResolution/settingsCam._pixelsToUnits;
			else
				return viewportResolution*_currentScale/settingsCam._pixelsToUnits; 
		}
	}
		
	public Vector2 worldPixelResolution
	{
		get {
			if(crop)
				return referenceResolution*_currentScale;
			else
				return viewportResolution*_currentScale; 
		}
	}

	bool  _cameraDirty = false;
	protected virtual void _UpdateCamera()
	{
		if( ! gameObject.activeInHierarchy || ! enabled)
		{
			return;
		}
		_cameraDirty = false;
		if( thisCamera == null ) thisCamera = camera;

		switch(fitMode)
		{
			case FitMode.fitWidth:
				_currentScale = referenceResolution.x / viewportResolution.x ;
				break;
			case FitMode.fitHeight:
				_currentScale = referenceResolution.y / viewportResolution.y ;
				break;
			case FitMode.fitEntire:
				if( referenceResolution.y / viewportResolution.y < referenceResolution.x / viewportResolution.x )
					_currentScale = referenceResolution.x / viewportResolution.x ;
				else
					_currentScale = referenceResolution.y / viewportResolution.y ;
				break;
			case FitMode.fitToFill:
				if( referenceResolution.y / viewportResolution.y > referenceResolution.x / viewportResolution.x )
					_currentScale = referenceResolution.x / viewportResolution.x ;
				else
					_currentScale = referenceResolution.y / viewportResolution.y ;
				break;
			case FitMode.none:
				_currentScale = 1;
				break;
		}

		if( crop )
			thisCamera.orthographicSize = viewportResolution.y * _currentScale* 0.5f;
		else
			thisCamera.orthographicSize = worldResolution.y * 0.5f;

		Vector3 newPosition;
		if (originIsBottomLeft) {
			newPosition = new Vector3(thisCamera.orthographicSize*thisCamera.aspect,thisCamera.orthographicSize,thisCamera.transform.position.z);
		} else {
			newPosition = new Vector3(0,0,thisCamera.transform.position.z);
		}
		if (thisCamera.transform.position!=newPosition) {
			thisCamera.transform.position = newPosition;
		}

		if( Camera.main == camera)
		{
			//print ("sending OnViewportChanged...");
			foreach( GameObject go in FindObjectsOfType<GameObject>())
			{
				go.SendMessage("OnViewportChanged",SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	public bool _crop = false;

	public bool crop
	{
		get { return settingsCam._crop; }
	}

	public bool _originIsBottomLeft = false;
	
	public bool originIsBottomLeft
	{
		get { return settingsCam._originIsBottomLeft; }
	}

	#if UNITY_EDITOR
	void OnGUI()
	{
		Update ();
	}
	#endif

	void Update()
	{
#if UNITY_EDITOR
		if( !Application.isPlaying )
#else
		if( _viewportResolution.x != Screen.width || _viewportResolution.y != Screen.height )
#endif
		{
			UpdateViewPortRez();
			_cameraDirty = true;
		}

		if( _cameraDirty )
		{
			_UpdateCamera();
		}

	}


	public static Material cropMaterial = null; 

	void OnPostRender()
	{
		if( crop )
		{
			if( ! cropMaterial )
			{
				cropMaterial = new Material(Shader.Find("Unlit/Color"));
				cropMaterial.color = Color.black;
			}
				
			Vector2 margins = viewportResolution - referenceResolution/_currentScale;
			//print ("margins "+margins);

			GL.PushMatrix();
			cropMaterial.SetPass(0);
			GL.LoadPixelMatrix();
			GL.Color(Color.black);
			if( margins.x > 0 )
			{
				GL.Begin(GL.QUADS);
				GL.Vertex3(0,0,0);
				GL.Vertex3(0,Screen.height,0);
				GL.Vertex3(margins.x*0.5f,Screen.height,0);
				GL.Vertex3(margins.x*0.5f,0,0);

				GL.Vertex3(Screen.width-margins.x*0.5f,0,0);
				GL.Vertex3(Screen.width-margins.x*0.5f,Screen.height,0);
				GL.Vertex3(Screen.width,Screen.height,0);
				GL.Vertex3(Screen.width,0,0);
				GL.End();
			}

			if( margins.y > 0 )
			{
				GL.Begin(GL.QUADS);
				GL.Vertex3(0,0,0);
				GL.Vertex3(0,margins.y*0.5f,0);
				GL.Vertex3(Screen.width,margins.y*0.5f,0);
				GL.Vertex3(Screen.width,0,0);
						
				GL.Vertex3(0,Screen.width,0);
				GL.Vertex3(Screen.width,Screen.height,0);
				GL.Vertex3(Screen.width,Screen.height-margins.y*0.5f,0);
				GL.Vertex3(0,Screen.height-margins.y*0.5f,0);
				GL.End();
			}
			GL.PopMatrix();
		}
	}

	/*
	CameraFade GetCameraFade()
	{
		CameraFade result = GetComponent<CameraFade>();
		if( result == null )
		{
			result = gameObject.AddComponent<CameraFade>();
		}

		return result;
	}

	public Color fadeColor = CameraFade.greyFadeColor;
	public float fadeTime = CameraFade.standardFadeTime; 
	public float blurLevel = 5;

	public bool FadeCompleted
	{
		get { 
			CameraFade result = GetComponent<CameraFade>();
			if( result == null ) return true;
			return result.FadeCompleted;
		}
	}

	public void FadeOut()
	{
		FadeOut(false);
	}

	public void FadeOut(bool blur)
	{
		FadeOut(blur,fadeTime);
	}

	public void FadeOut(bool blur,float time)
	{
		FadeOut(blur,time,fadeColor);
	}

	public void FadeOut(bool blur,float time, Color c)
	{
		CameraFade fade = GetCameraFade();
	#if USE_BLUR
		if( blur )
		{
			Blur blurComp = GetComponent<Blur>();
			if( blurComp != null )
			{
				blurComp.enabled = true;
			}
		}
	#endif
		fade.FadeOut(time,c,blurLevel);
		WidgetManager2D wm = GetComponent<WidgetManager2D>();
		if( wm )
		{
			wm.enabled = false;
		}
	}

	public void FadeIn()
	{
		FadeIn(fadeTime);
	}

	public void FadeIn(float time)
	{
		CameraFade fade = GetCameraFade();
		fade.FadeIn(time);
		WidgetManager2D wm = GetComponent<WidgetManager2D>();
		if( wm )
		{
			wm.enabled = true;
		}
	}
	*/

	#if UNITY_EDITOR

	void OnValidate()
	{
		if( !Application.isPlaying )
		{
			CheckInheritLoop();
			_UpdateCamera();
		}
	}

	void LateUpdate()
	{
		if( !Application.isPlaying )
		{
			CheckInheritLoop();
			_UpdateCamera();
		}
	}

	#endif

}
